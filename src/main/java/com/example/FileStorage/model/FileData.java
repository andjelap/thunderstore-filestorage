package com.example.FileStorage.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FileData {
    private String filename;
    private String url;
    private Long size;

}
